# Almond IntelliJ plugin

## 0.0.4

### Bugs

- Detect multiple "define" and "require" on the same file.
- Not show compile error when the reference cannot be resolved.

### Features

- Navigation from define declaration to its usage. (Cannot detect string usage e.g. json)


## 0.0.3

### Improvement

- Added Reference contributor, refactoring doesn't work though
- Code is a mess, yet cleaned them up


## 0.0.2

### Bugs

- Fixed problem with single quote, double quote
- Use 14.0 API version of Javascript instead of 14.1 (caused search failures on 14.0 before)

### Features

- Added file name to the search result
- Limited the search result to 20, it won't go nuts
- Search result is now project related (0.0.1, the search result contains result from other projects as well)

## 0.0.1

### Features

- Index js files based on `define` statement
- Suggestion list for `require` and `define`

Follow [this link](https://bitbucket.org/atlassian/almond-intellij-plugin) for feature request, I will try to tackle in the upcoming innovation week :)
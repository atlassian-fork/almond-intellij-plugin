package com.atlassian.intellij.plugin.almond.model;

import com.intellij.openapi.vfs.VirtualFile;

public class JSDefineIndexSearch
{
    private final String key;
    private final VirtualFile file;

    public JSDefineIndexSearch(String key, VirtualFile file)
    {
        this.key = key;
        this.file = file;
    }
}

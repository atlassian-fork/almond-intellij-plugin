package com.atlassian.intellij.plugin.almond.indexer;

import com.intellij.util.indexing.DataIndexer;
import com.intellij.util.indexing.FileContent;
import gnu.trove.THashMap;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSDefineDataIndexer implements DataIndexer<String, Void, FileContent>
{
    private static final Pattern DEFINE_REGEX = Pattern.compile("define[ \\t\\r\\n]*\\([ \\t\\r\\n]*[\"'](.*?)[\"']");

    @NotNull
    @Override
    public Map<String, Void> map(@NotNull FileContent inputData)
    {
        final Map<String, Void> cache = new THashMap<String, Void>();

        Matcher matcher = DEFINE_REGEX.matcher(inputData.getContentAsText());
        while (matcher.find())
        {
            String key = matcher.group(1).trim();
            cache.put(key, null);
        }
        return cache;
    }

}

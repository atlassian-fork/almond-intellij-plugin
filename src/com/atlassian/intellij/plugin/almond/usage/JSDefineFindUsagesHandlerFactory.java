package com.atlassian.intellij.plugin.almond.usage;

import com.atlassian.intellij.plugin.almond.utils.ElementPatterns;
import com.intellij.lang.javascript.findUsages.JavaScriptFindUsagesHandlerFactory;
import com.intellij.lang.javascript.psi.JSLiteralExpression;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

public class JSDefineFindUsagesHandlerFactory extends JavaScriptFindUsagesHandlerFactory {

    public boolean canFindUsages(@NotNull PsiElement element) {
        return element instanceof JSLiteralExpression && ElementPatterns.isDefineDeclaration(element);
    }

}

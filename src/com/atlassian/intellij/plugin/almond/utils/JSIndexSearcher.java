package com.atlassian.intellij.plugin.almond.utils;

import com.atlassian.intellij.plugin.almond.model.Collector;
import com.atlassian.intellij.plugin.almond.model.JSIndexSearchResult;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Set;

import static com.atlassian.intellij.plugin.almond.indexer.JSDefineIndex.ALMOND_INDEXER_ID;
import static com.atlassian.intellij.plugin.almond.indexer.JSRequireIndex.REQUIRE_INDEXER_ID;

public class JSIndexSearcher
{
    public static Set<JSIndexSearchResult> getRequires(final String keyword, final Project project)
    {
        final FileBasedIndex indexer = FileBasedIndex.getInstance();
        Collection<VirtualFile> files = indexer.getContainingFiles(REQUIRE_INDEXER_ID, keyword, GlobalSearchScope.allScope(project));
        Set<JSIndexSearchResult> result = Sets.newHashSet();
        for (VirtualFile file : files)
        {
            result.add(new JSIndexSearchResult(file, keyword));
        }
        return result;
    }

    public static Set<JSIndexSearchResult> get(final String keyword, final Project project)
    {
        final FileBasedIndex indexer = FileBasedIndex.getInstance();
        Collection<VirtualFile> files = indexer.getContainingFiles(ALMOND_INDEXER_ID, keyword, GlobalSearchScope.allScope(project));
        Set<JSIndexSearchResult> result = Sets.newHashSet();
        for (VirtualFile file : files)
        {
            result.add(new JSIndexSearchResult(file, keyword));
        }
        return result;
    }

    public static Set<JSIndexSearchResult> find(final String keyword, final Project project)
    {
        final FileBasedIndex indexer = FileBasedIndex.getInstance();

        Collection<String> values = indexer.getAllKeys(ALMOND_INDEXER_ID, project);
        final Set<JSIndexSearchResult> result = Sets.newHashSet();
        Collector.consume(values, new Predicate<String>() {
            @Override
            public boolean apply(final String key) {
                if (StringUtils.containsIgnoreCase(key, keyword)) {
                    Collection<VirtualFile> files = indexer.getContainingFiles(ALMOND_INDEXER_ID, key, GlobalSearchScope.projectScope(project));
                    for (VirtualFile file : files) {
                        result.add(new JSIndexSearchResult(file, key));
                    }
                    return !files.isEmpty();
                } else {
                    return false;
                }
            }
        }, 20);

        return result;
    }

    public static Set<JSIndexSearchResult> list(final Project project)
    {
        final FileBasedIndex indexer = FileBasedIndex.getInstance();

        Collection<String> values = indexer.getAllKeys(ALMOND_INDEXER_ID, project);
        final Set<JSIndexSearchResult> result = Sets.newHashSet();
        Collector.consume(values, new Predicate<String>()
        {
            @Override
            public boolean apply(final String key)
            {
                Collection<VirtualFile> files = indexer.getContainingFiles(ALMOND_INDEXER_ID, key, GlobalSearchScope.projectScope(project));
                for (VirtualFile file : files)
                {
                    result.add(new JSIndexSearchResult(file, key));
                }

                return !files.isEmpty();
            }
        }, 20);

        return result;
    }

}

package com.atlassian.intellij.plugin.almond.utils;

import com.atlassian.intellij.plugin.almond.model.JSIndexSearchResult;
import com.google.common.collect.Lists;
import com.intellij.lang.javascript.JavascriptLanguage;
import com.intellij.lang.javascript.psi.JSCallExpression;
import com.intellij.lang.javascript.psi.JSExpression;
import com.intellij.lang.javascript.psi.JSLiteralExpression;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.patterns.PsiElementPattern;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.util.PsiTreeUtil;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class ElementPatterns
{
    public static PsiElementPattern.Capture<PsiElement> requirePattern =
            PlatformPatterns.psiElement()
                    .withSuperParent(3, PlatformPatterns.psiElement(JSCallExpression.class))
                    .withLanguage(JavascriptLanguage.INSTANCE);

    public static PsiElementPattern.Capture<PsiElement> definePattern =
            PlatformPatterns.psiElement()
                    .withSuperParent(4, PlatformPatterns.psiElement(JSCallExpression.class))
                    .withLanguage(JavascriptLanguage.INSTANCE);

    public static boolean isDefineDeclaration(PsiElement literalExpression)
    {
        try
        {
            PsiElement callExpressionElement = literalExpression.getParent().getParent();
            JSCallExpression callExpression = (JSCallExpression) callExpressionElement;
            String method = callExpression.getMethodExpression().getText();
            return method.equals("define");
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public static boolean isDefineCallExpression(PsiElement literalExpression)
    {
        PsiElement arrayLiteralExpression = literalExpression.getParent();
        PsiElement argumentList = arrayLiteralExpression.getParent();
        PsiElement callExpressionElement = argumentList.getParent();

        if (callExpressionElement instanceof JSCallExpression)
        {
            JSCallExpression callExpression = (JSCallExpression) callExpressionElement;
            String method = callExpression.getMethodExpression().getText();
            if (method.equals("define"))
            {
                return true;
            }
        }
        return false;
    }

    public static boolean isRequireCallExpression(PsiElement literalExpression)
    {
        PsiElement argumentList = literalExpression.getParent();
        PsiElement callExpressionElement = argumentList.getParent();

        if (callExpressionElement instanceof JSCallExpression)
        {
            JSCallExpression callExpression = (JSCallExpression) callExpressionElement;
            String method = callExpression.getMethodExpression().getText();
            if (method.equals("require"))
            {
                return true;
            }
        }
        return false;
    }

    public static List<PsiElement> getDefines(Set<JSIndexSearchResult> results, Project project)
    {
        List<PsiElement> finalResult = Lists.newArrayList();
        for (JSIndexSearchResult result : results)
        {
            VirtualFile file = result.getVirtualFile();
            String keyword = result.getKey();

            PsiFile psifile = PsiManager.getInstance(project).findFile(file);

            Collection<JSCallExpression> callExpressions = PsiTreeUtil.findChildrenOfType(psifile, JSCallExpression.class);

            for (JSCallExpression callExpression : callExpressions)
            {
                if (callExpression.getMethodExpression().getText().equals("define"))
                {
                    JSExpression[] arguments = callExpression.getArguments();
                    if (arguments.length > 0)
                    {
                        JSExpression argument0 = arguments[0];
                        if (argument0 instanceof JSLiteralExpression
                                && ((JSLiteralExpression) argument0).isQuotedLiteral()
                                && StringUtil.unquoteString(argument0.getText()).equals(keyword))
                        {
                            finalResult.add(argument0);
                        }
                    }
                }
            }
        }

        return finalResult;
    }

    public static List<PsiElementResolveResult> getRequireReferences(Set<JSIndexSearchResult> results, Project project)
    {
        List<PsiElementResolveResult> finalResult = Lists.newArrayList();
        for (JSIndexSearchResult result : results) {
            VirtualFile file = result.getVirtualFile();
            String keyword = result.getKey();
            PsiFile psifile = PsiManager.getInstance(project).findFile(file);
            Collection<JSLiteralExpression> literalExpressions = PsiTreeUtil.findChildrenOfType(psifile, JSLiteralExpression.class);
            for (JSLiteralExpression exp : literalExpressions) {
                if (exp.isQuotedLiteral() && keyword.equals(StringUtil.unquoteString(exp.getText())) &&
                        (isDefineCallExpression(exp) || isRequireCallExpression(exp)))
                {
                    finalResult.add(new PsiElementResolveResult(exp));
                }
            }

        }
        return finalResult;
    }

}
